const http = require('http');

http.createServer(function(req, res){
    // The method GET means that we will be retrieving or reading information.
    if (req.url == "/items" && req.method == "GET") {
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.end("Data retrieved from database");
    } 

    // The method POST means that we will be adding or creating information.
    if (req.url == "/items" && req.method == "POST") {
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.end("Data to be sent to the database");
    } 
}).listen(4000);
console.log("System is now running at localhost 4000");