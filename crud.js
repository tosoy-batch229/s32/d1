const http = require('http');

// Dummy Data
let directory = [
    {
        "name" : "Brandon",
        "email" : "brandon@mail.com",
        
    },
    {
        "name" : "Jobert",
        "email" : "jobert@mail.com",
    }
]

http.createServer(function(req, res){
    // Route for returning all items upon recieving GET method.
    if (req.url == "/users" && req.method == "GET") {
        res.writeHead(200, {"Content-Type" : "application/json"});
        // Input has to be data type STRING for the application to read the incoming data properly.
        res.write(JSON.stringify(directory));
        // Ends the response process.
        res.end();
    }

    if (req.url == "/users" && req.method == "POST") {
        // Declare and initialize reqBody variable to an empty string.
        // This will act as a placeholder for the data to be created later.
        let reqBody = "";

        // This is where data insertion happens to our dummy data.
        req.on("data", function(data) {
            // Assigns the data retrieved from the stream to reqBody.
            reqBody += data;
        });

        // res end stops - only runs after the request has completely been set.
        req.on("end", function() {
            console.log(typeof reqBody);

            // converts the strings to JSON.
            reqBody = JSON.parse(reqBody);

            // Create a new object representing the new dummy data record.
            let newUser = {
                "name" : reqBody.name,
                "email" : reqBody.email
            }

            directory.push(newUser);
            console.log(directory);

            res.writeHead(200, {"Content-Type" : "application/json"});
            res.write(JSON.stringify(newUser));
            res.end();
        });
    }
}).listen(3000);
console.log("CRUD is now running at port 3000");